//
//  AmenitiesCollectionCell.swift
//  NodomPMTenant
//
//  Created by MAC on 27/03/21.
//

import UIKit

class AmenitiesCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var IconImageView: UIImageView!
    @IBOutlet weak var amenitiesLabel: CustomLabel!
}
