//
//  HistoricHeaderCell.swift
//  NodomLandlord
//
//  Created by Madhuraj on 13/05/21.
//

import UIKit

class HistoricHeaderCell: UITableViewCell {

    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerButton: UIButton!
    @IBOutlet weak var headerIcon: ExpandableIcon!
    @IBOutlet weak var amountLabel: UILabel!
    
}
