//
//  NotificationHeaderCell.swift
//  NodomLandlord
//
//  Created by Madhuraj on 17/04/21.
//

import UIKit

class NotificationHeaderCell: UITableViewCell {

    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerButton: UIButton!
    @IBOutlet weak var headerIcon: ExpandableIcon!
    
}
