//
//  NotificationsCell.swift
//  NodomTenant
//
//  Created by MAC on 19/02/21.
//

import UIKit

class NotificationsCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var notificaitonImageView: UIImageView!
    
    
}
