//
//  OfferNotificationCell.swift
//  NodomLandlord
//
//  Created by Madhuraj on 17/04/21.
//

import UIKit

class OfferNotificationCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var propNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
}
