//
//  PaymentCell.swift
//  NodomLandlord
//
//  Created by Madhuraj on 24/02/21.
//

import UIKit

class PaymentCell: UITableViewCell {

    
    @IBOutlet weak var propName: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var transactionID: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    
}
