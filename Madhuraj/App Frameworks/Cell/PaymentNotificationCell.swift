//
//  PaymentNotificationCell.swift
//  NodomLandlord
//
//  Created by Madhuraj on 17/04/21.
//

import UIKit

class PaymentNotificationCell: UITableViewCell {

    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    

}
