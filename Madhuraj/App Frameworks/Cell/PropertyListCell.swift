//
//  PropertyListCell.swift
//  NodomPMTenant
//
//  Created by MAC on 27/03/21.
//

import UIKit

class PropertyListCell: UITableViewCell {

    @IBOutlet weak var propImageView: UIImageView!
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var addressLabel: CustomLabel!
    @IBOutlet weak var rentLabel: CustomLabel!
    @IBOutlet weak var leaseEndLabel: CustomLabel!
    @IBOutlet weak var areaLabel: UILabel!
    @IBOutlet weak var bedLabel: UILabel!
    @IBOutlet weak var floorLabel: UILabel!
    @IBOutlet weak var bathLabel: UILabel!
    @IBOutlet weak var bedView: UIView!
    @IBOutlet weak var floorView: UIView!
    @IBOutlet weak var bathView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        propImageView.roundCorners(corners: [.topLeft, .bottomLeft], radius: 15)
    }

    

}
