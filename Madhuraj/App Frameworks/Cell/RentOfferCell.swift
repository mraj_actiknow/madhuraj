//
//  RentOfferCell.swift
//  NodomLandlord
//
//  Created by Madhuraj on 22/02/21.
//

import UIKit

class RentOfferCell: UITableViewCell {

    @IBOutlet weak var propImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var propNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var rentLabel: UILabel!
    @IBOutlet weak var leaseEndLabel: UILabel!
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var sqFeetLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    

}
