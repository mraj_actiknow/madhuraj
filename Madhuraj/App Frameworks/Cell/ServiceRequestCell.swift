//
//  ServiceRequestCell.swift
//  NodomLandlord
//
//  Created by Madhuraj on 23/02/21.
//

import UIKit

class ServiceRequestCell: UITableViewCell {

    @IBOutlet weak var approvalImageView: ApprovalImageView!
    @IBOutlet weak var approvalLabel: ApprovalLabel!
    @IBOutlet weak var approvalView: ApprovalView!
    @IBOutlet weak var statusHeight: NSLayoutConstraint!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
