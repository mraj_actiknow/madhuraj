//
//  Constants.swift
//  NodomLandlord
//
//  Created by Madhuraj on 17/03/21.
//

import Foundation

struct Constants {
    struct URL {
        static let base = "https://itunes.apple.com/search?term=";
    }
    
    struct  APIName {
       static let mjAPi = "Michael+jackson"
    }
    
    
    struct Params {
        static let results = "results"
    }
    
    struct UserDefaultKeys {
        
    }
    
    struct StringConstants {
        static let error = "error"
        static let message = "message"
    }
    
    struct SuccessMessage {
        static let success = "List Fetched Successfully"
    }

    struct ErrorMessage {
        static let unknown = "Something went wrong!"
      
    }
    
    
}
