//
//  CellModelProtocols.swift
//  HomeTrust
//
//  Created by Madhuraj on 05/01/21.
//

import UIKit

// MARK: - Cell Model -
protocol CellModelProtocol {
    // Reuse identifier
    var reuseIdentifier: String {get set}
    // Variable height support
    var height: CGFloat {get set}
    var estimatedhHeight: CGFloat {get set}
}

// MARK: - Configuration -
protocol ConfigurableView {
    func configure(_ cellModel:CellModelProtocol)
}

// MARK: - Registration (Identifier) -
protocol ReusableView {
    static var identifier:String { get }
}

extension ReusableView where Self: UITableViewCell {
    static var identifier: String {
        return String(describing: self)
    }
}

// MARK: - Registration (NibName) -
protocol NibLoadableView {
    static var nibName:String {get}
}

extension NibLoadableView {
    static var nibName:String {
        return String(describing: self)
    }
}

// MARK: - UITableViewCell Extension -
extension UITableViewCell : ReusableView {
}

extension UITableViewCell : NibLoadableView {
}

// MARK: - UITableView Extension -
extension UITableView {
    func resgisterCell<T: UITableViewCell>(_: T.Type) where T: ReusableView, T: NibLoadableView {
        register(UINib(nibName: T.nibName, bundle: Bundle(for:T.self)), forCellReuseIdentifier: T.identifier)
    }

    func registerHeaderFooterView<T: UITableViewHeaderFooterView>(_: T.Type) where T: ReusableView, T: NibLoadableView {
        register(UINib(nibName: T.nibName, bundle: Bundle(for:T.self)), forHeaderFooterViewReuseIdentifier: T.identifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(for indexPath:IndexPath) -> T where T: ReusableView {
        if let cell = self.dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as? T {
            return cell
        }
        fatalError("Could not dequeue cell with identifier: \(T.identifier)")
    }
}
