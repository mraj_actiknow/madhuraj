//
//  Extensions.swift
//  ClearSale
//
//  Created by Indra Mohan on 27/05/17.
//  Copyright © 2017 Actiknow. All rights reserved.
//

import UIKit
import Foundation

extension CGSize {
    static var screenSize = UIScreen.main.bounds.size
}


extension CGPath {
    static func squircle(of rect: CGRect) -> CGPath {
        return CGPath(roundedRect: rect, cornerWidth: rect.width/4, cornerHeight: rect.height/4, transform: nil)
    }
}

extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}



extension UIView {
    func shake() {
        let anim = CAKeyframeAnimation(keyPath: "transform")
        anim.values = [NSValue(caTransform3D: CATransform3DMakeTranslation(-5.0, 0.0, 0.0)), NSValue(caTransform3D: CATransform3DMakeTranslation(5.0, 0.0, 0.0))];
        anim.autoreverses = true
        anim.repeatCount = 2
        anim.duration = 0.07
        layer.add(anim, forKey: "shake")
    }
    
    func rotate(by degree:CGFloat, in duration:TimeInterval = 0.25) {
        if duration > 0 {
            UIView.animate(withDuration: duration, animations:{
                let angle = (CGFloat(Float.pi) / 180.0) * degree
                self.transform = CGAffineTransform(rotationAngle: angle)
            })

        } else {
            let angle = (CGFloat(Float.pi) / 180.0) * degree
            self.transform = CGAffineTransform(rotationAngle: angle)
        }
    }
    
    func addGradient(frame: CGRect, color:UIColor = UIColor.black, isTop:Bool = false) {
        let gradient = CAGradientLayer()
        gradient.frame = frame
        if isTop {
            gradient.colors = [color.withAlphaComponent(0.8).cgColor, UIColor.clear.cgColor]
        } else {
            gradient.colors = [UIColor.clear.cgColor, color.withAlphaComponent(0.8).cgColor]
        }
        gradient.locations = [0.0, 0.6]
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    
    func removeShdadow() {
        layer.shadowColor = nil
        layer.shadowOpacity = 0
        layer.shadowRadius = 3
        layer.shadowOffset = CGSize(width: 0, height: -3)
    }
    

}
extension UIScrollView {
    func scrollToPage(page: Int, animated: Bool) {
        var frame: CGRect = self.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        self.scrollRectToVisible(frame, animated: animated)
    }
}
extension String {
    func attributed(font:UIFont? = nil, color:UIColor? = nil) -> NSAttributedString {
        var attributes = [NSAttributedString.Key:Any]()
        if let font = font  {
            attributes[NSAttributedString.Key.font] = font
        }
        if let color = color {
            attributes[NSAttributedString.Key.foregroundColor] = color
        }
        return NSAttributedString(string: self, attributes: attributes)
    }
    
    var underLine:NSAttributedString {
        return NSAttributedString(string: self, attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
    }
    
    var formattedHTML:String {
        return self.replacingOccurrences(of: "myFont", with: "Graphik-Regular")
    }
    
    var isValidEmail:Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    var trimmed:String {
        return (self as NSString).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    var attributedString:NSMutableAttributedString? {
        do {
            let string = self.replacingOccurrences(of: "font-family: myFont", with: "font-family: Graphik-Regular")
//                             .replacingOccurrences(of: "text-align: justify;", with: "text-align: left;")
//                             .replacingOccurrences(of: "color: rgb(51, 51, 51);", with: "color: rgb(19, 19, 19);")
//                             .replacingOccurrences(of: "color:#616161;", with: "color: rgb(19, 19, 19);")
            
            let attr = try NSMutableAttributedString(data: string.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            //attr.trim(charactersIn: "\t\n\r")
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineBreakMode = .byWordWrapping
            paragraphStyle.alignment = .left
            paragraphStyle.lineSpacing = 3
            paragraphStyle.paragraphSpacing = 5
            let range = NSRange(location: 0, length: attr.string.count)
            attr.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: range)
            
            return attr
        } catch {
            return nil
        }
    }
}

extension UIWindow {
    class func topWindow() -> UIWindow? {
        for window in UIApplication.shared.windows.reversed() {
            if window.windowLevel == UIWindow.Level.normal && !window.isHidden && window.frame != CGRect.zero { return window }
        }
        return nil
    }
}
extension UILabel {
  func set(html: String) {
    if let htmlData = html.data(using: .unicode) {
      do {
        self.attributedText = try NSAttributedString(data: htmlData,
                                                     options: [.documentType: NSAttributedString.DocumentType.html],
                                                     documentAttributes: nil)
      } catch let e as NSError {
        print("Couldn't parse \(html): \(e.localizedDescription)")
      }
    }
  }
}
extension UIApplication {
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            if application.canOpenURL(URL(string: url)!) {
                application.open(URL(string: url)!, options: [:], completionHandler: nil)
                return
            }
        }
    }
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
