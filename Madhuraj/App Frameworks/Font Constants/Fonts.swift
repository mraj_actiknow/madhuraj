//
//  Fonts.swift
//  CarApp
//
//  Created by Saumya on 17/12/20.
//

import UIKit

enum TextSize: CGFloat {

    case HEADING = 30.0
    case SUBHEADING = 28.0
    case TITLE = 24.0
    case SUBTITLE = 20.0
    case EXTRA_LARGE = 18.0
    case LARGE = 16.0
    case NORMAL = 14.0
    case SMALL = 12.0
    case TINY = 9.0
}

enum TextTypeface: String{
    case REGULAR = "DINNextLTPro-Regular"
    case BOLD = "DINNextLTPro-Bold"
}
