//
//  ServiceHelper.swift
//
//  Created by Madhuraj on 11/20/2018.
//

import Foundation
import Alamofire

typealias progressBlock = (_ progress: Double) -> Void //2
typealias completionBlock = (_ response: Any?, _ error: Error?) -> Void //3

final class ServiceHelper {
    
    //shared instance of Class
    class var sharedInstanceHelper: ServiceHelper {
        struct Static {
            static let instance = ServiceHelper()
        }
        return Static.instance
    }
    
    
    /// Create Request for webservice
    ///
    /// - Parameters:
    ///   - method: request type (post, get, put)
    ///   - params: request parameters
    ///   - apiName: api name to create url
    ///   - completionHandler: closure
    
    
    func createRequest(method: String,showHud :Bool, params: [String: AnyObject]!, apiName: String, completionHandler:@escaping (_ response: AnyObject?, _ error: NSError?) -> Void) {
        let parameters = params
        let url = Constants.URL.base + apiName
       
        AF.request(url, method: HTTPMethod(rawValue: method), parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    completionHandler(JSON as AnyObject?, nil)
                }
            case .failure(let error):
            print(error as AnyObject? as Any)
            completionHandler(nil, error as NSError?)
            }
        }
        
    }
    
    func createRawRequest(apiName: String, params: [String: AnyObject]!, completionHandler:@escaping (_ response: AnyObject?, _ error: NSError?) -> Void) {
        let urlString = Constants.URL.base + apiName
        guard let url = URL(string: urlString) else {return}
        var request        = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            request.httpBody   = try JSONSerialization.data(withJSONObject: params!)
        } catch let error {
            print("Error : \(error.localizedDescription)")
        }
        AF.request(request).responseJSON{ response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    
                    completionHandler(JSON as AnyObject?, nil)
                }
            case .failure(let error):
            print(error as AnyObject? as Any)
            completionHandler(nil, error as NSError?)
            }
        }
    }
    
    
    /****************************
     * Function Name : - uploadDocumentToS3
     * Create on : - 26th March 2021
     * Developed By : - MADHURAJ
     * Description : - This method is used for Upload image with thumbnail in 200x200 size.
     * Organisation Name :- Actiknow
     * version no :- 1.0
     ****************************/
    
    
   
}


