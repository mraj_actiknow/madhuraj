//
//  APIServices.swift
//  ZPI-CASH
//
//  Created by Ajath Infotech on 09/04/19.
//  Copyright © 2019 Zerone Microsystems. All rights reserved.
//

import Foundation
import UIKit

final class APIServices {
    let myGroup = DispatchGroup()
    //shared instance of Class
    static let sharedInstance = APIServices()
    
    // Get MJ List
    func getList(params: [String: AnyObject], completion:@escaping (_ result: [MJData], _ status: Bool) -> Void) {
        ServiceHelper.sharedInstanceHelper.createRequest(method: "GET", showHud: true, params:params as [String : AnyObject], apiName: Constants.APIName.mjAPi) { (result, error) in
            if(error == nil){
                print(result as Any)
                guard (result as? [String: Any] ?? [:])[Constants.Params.results] != nil else {
                    Toast.show(message: Constants.ErrorMessage.unknown)
                    return
                }
                let list = (result as! [String: Any])[Constants.Params.results] as! [[String: Any]]
                var responseModel: [MJData] = []
                for data in list {
                    responseModel.append(MJData(dictionary: data as NSDictionary)!)
                }
                completion(responseModel, true)
            } else {
                completion([], false)
            }
        }
    }
   
    
}
