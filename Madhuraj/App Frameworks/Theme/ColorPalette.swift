//
//  ColorPalette.swift
//  NodomLandlord
//
//  Created by Madhuraj on 24/03/21.
//

import Foundation
import UIKit

// MARK: - ColorPalette -
extension UIColor {
    static var navigationBarColor: UIColor {
        return UIColor(red: 0.00, green: 0.32, blue: 0.86, alpha: 1.00)
    }
    
    static var backgroundColor: UIColor {
        return UIColor(red: 0.80, green: 0.88, blue: 1.00, alpha: 1.00)
    }
    
    static var labelColor: UIColor {
        return UIColor(red: 0.00, green: 0.32, blue: 0.86, alpha: 1.00)
    }
}
