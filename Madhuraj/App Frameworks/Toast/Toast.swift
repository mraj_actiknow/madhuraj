//
//  Toast.swift
//  Madhuraj
//
//  Created by Madhuraj on 26/07/21.
//

import UIKit
import Toast_Swift

struct Toast {
    
    static func show(message:String) {
        UIWindow.topWindow()?.makeToast(message)
    }
}
