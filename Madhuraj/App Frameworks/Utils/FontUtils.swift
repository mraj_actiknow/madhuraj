//
//  FontUtils.swift
//  CarApp
//
//  Created by Saumya on 17/12/20.
//

import UIKit

class FontUtils: NSObject {

    static func getFont(typeface: TextTypeface, size: TextSize)->UIFont?{
        return UIFont(name: typeface.rawValue, size: size.rawValue);
    }
    
    static func getFont(typeface: String, size: String)->UIFont?{
        return getFont(typeface: getTypeface(typeface: typeface), size: getFontSize(fontSize: size));
    }
    
    static func getSystemFont(size: TextSize, weight: UIFont.Weight)->UIFont?{
        return UIFont.systemFont(ofSize: size.rawValue, weight: weight)
    }
    
    static fileprivate func getFontSize(fontSize: String) -> TextSize{
        switch fontSize.lowercased(){
        case "title":
            return TextSize.TITLE
        case "subtitle":
            return TextSize.SUBTITLE
        case "heading":
            return TextSize.HEADING
        case "subheading":
            return TextSize.SUBHEADING
        case "extralarge":
            return TextSize.EXTRA_LARGE
        case "large" :
            return TextSize.LARGE
        case "normal":
            return TextSize.NORMAL
        case "small":
            return TextSize.SMALL
        case "tiny":
            return TextSize.TINY
        default:
            return TextSize.NORMAL
        }
    }
    
    static fileprivate func getTypeface(typeface: String) -> TextTypeface{
        switch typeface.lowercased(){
        case "regular","normal":
            return TextTypeface.REGULAR
        case "bold" :
            return TextTypeface.BOLD
        default:
            return TextTypeface.REGULAR
        }
    }
}
