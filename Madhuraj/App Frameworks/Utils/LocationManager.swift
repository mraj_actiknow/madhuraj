//
//  LocationManager.swift
//  beatroute
//
//  Created by Saumya on 17/12/20.
//

import UIKit
import CoreLocation

protocol LocationDetailsDelegate{
    func onReceivingLocationDetails(city: String, pincode: String, state: String, country: String, district: String)
}

class LocationManager: CLLocationManager, CLLocationManagerDelegate{
    
    public static let locationManager = LocationManager()
    
    var latitude: CLLocationDegrees?
    var longitude: CLLocationDegrees?
    var locationDetailsDelegate : LocationDetailsDelegate?

    var strCity : String?
    var strPincode : String?
    var strState : String?
    var strDistrict : String?
    var strCountry : String?

    static func start(){
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self.locationManager
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    static func calculateDistance(lat: String?, long: String?) -> CLLocationDistance
    {
        let userCoordinates = CLLocation(latitude: LocationManager.locationManager.latitude!, longitude: LocationManager.locationManager.longitude!)
        let customerCoordinates = CLLocation(latitude: CLLocationDegrees(lat!)!, longitude: CLLocationDegrees(long!)!)
        return userCoordinates.distance(from: customerCoordinates)/1000
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude), \(locValue.longitude)")
        LocationManager.locationManager.latitude = locValue.latitude
        LocationManager.locationManager.longitude = locValue.longitude
//        DataServices.sharedInstance.userLattitude = String(locValue.latitude)
//        DataServices.sharedInstance.userLongitude = String(locValue.longitude)
        
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: locValue.latitude, longitude: locValue.longitude), completionHandler: { (placemarks, _) -> Void in
            
            placemarks?.forEach { (placemark) in
                if let city = placemark.locality { self.strCity = city }
                if let pincode = placemark.postalCode { self.strPincode = pincode }
                if let state = placemark.administrativeArea { self.strState = state }
                if let district = placemark.subAdministrativeArea { self.strDistrict = district }
                if let country = placemark.country { self.strCountry = country }
            }
        })
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        LocationManager.locationManager.stopUpdatingLocation()
        print(error.localizedDescription)
    }
    
    static func checkLocationStatus(){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    func getLocationDetails(){
        if let delegate = locationDetailsDelegate{
            delegate.onReceivingLocationDetails(city: strCity ?? "", pincode: strPincode ?? "", state: strState ?? "", country: strCountry ?? "", district: strDistrict ?? "")
        }
    }
    
    static func updateLocationDetails(){
        locationManager.getLocationDetails()
    }
    
}
