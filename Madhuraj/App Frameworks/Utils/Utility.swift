//
//  Utility.swift
//  Madhuraj
//
//  Created by Madhuraj on 26/07/21.
//

import Foundation
import UIKit
class Utility {

    static func delay(sec: Int, completion: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(sec)) {
            completion()
        }
    }
}
