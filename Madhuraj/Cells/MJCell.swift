//
//  MJCell.swift
//  Madhuraj
//
//  Created by Madhuraj on 26/07/21.
//

import UIKit

class MJCell: UITableViewCell {

    @IBOutlet weak var mjImageView: UIImageView!
    @IBOutlet weak var titleMJLabel: TitleMJLabel!
    @IBOutlet weak var artistNameLabel: DescriptionLabel!
    @IBOutlet weak var durationLabel: DescriptionLabel!
    
    
    
}
