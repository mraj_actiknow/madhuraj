//
//  AppIconImageView.swift
//  Madhuraj
//
//  Created by Madhuraj on 26/07/21.
//

import UIKit

class AppIconImageView: UIImageView {

    override  func awakeFromNib() {
        self.image = UIImage(named: "app")
    }

}
