//
//  DescriptionLabel.swift
//  Madhuraj
//
//  Created by Madhuraj on 26/07/21.
//

import UIKit

class DescriptionLabel: UILabel {

    override func awakeFromNib() {
        self.font = UIFont.systemFont(ofSize: 12)
        self.textColor = UIColor.labelColor
    }
}
