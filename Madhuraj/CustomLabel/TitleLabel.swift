//
//  TitleLabel.swift
//  Madhuraj
//
//  Created by Madhuraj on 26/07/21.
//

import UIKit

class TitleMJLabel: UILabel {
    override func awakeFromNib() {
        self.font = UIFont.boldSystemFont(ofSize: 15)
        self.textColor = UIColor.labelColor
    }
}
