//
//  MJTableView.swift
//  Madhuraj
//
//  Created by Madhuraj on 26/07/21.
//

import UIKit

class MJTableView: UITableView {

    var mjlList: [MJData]? = [] {
        didSet{
            self.setupUI()
        }
    }
    fileprivate func setupUI(){
        //self.register(MJCell.self, forCellReuseIdentifier: "MJCell")
        self.delegate = self
        self.dataSource = self
        self.separatorStyle = .none
        self.allowsSelection = false
        self.reloadData()
        self.backgroundColor = UIColor.backgroundColor
    }
    
}
extension MJTableView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mjlList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MJCell") as! MJCell
        let data = mjlList![indexPath.row]
        cell.titleMJLabel.text = "\(data.trackCensoredName ?? "") \(data.trackExplicitness ?? "") \(data.trackName ?? "")"
        cell.artistNameLabel.text = data.artistName
        cell.durationLabel.text = "\((data.trackTimeMillis ?? 0)/1000) Sec"
        return cell
    }
    
}
