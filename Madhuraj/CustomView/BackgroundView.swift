//
//  BackgroundView.swift
//  Madhuraj
//
//  Created by Madhuraj on 26/07/21.
//

import UIKit

class BackgroundView: UIView {

    override func awakeFromNib() {
        self.backgroundColor = UIColor.backgroundColor
    }

}
