//
//  NavBar.swift
//  Madhuraj
//
//  Created by Madhuraj on 26/07/21.
//

import UIKit

class NavBar: UIView {
    let titleLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    override func awakeFromNib() {
        self.backgroundColor = UIColor.navigationBarColor
        let rect = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        titleLabel.frame = rect
        addSubview(titleLabel)
    }
    
    var titleText: String? = "" {
        didSet {
            titleLabel.text = titleText
        }
    }
    
    

}
