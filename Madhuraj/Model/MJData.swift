/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class MJData {
	public var wrapperType : String?
	public var kind : String?
	public var trackId : Int?
	public var artistName : String?
	public var trackName : String?
	public var trackCensoredName : String?
	public var trackViewUrl : String?
	public var previewUrl : String?
	public var artworkUrl30 : String?
	public var artworkUrl60 : String?
	public var artworkUrl100 : String?
	public var trackRentalPrice : Double?
	public var trackHdRentalPrice : Double?
	public var releaseDate : String?
	public var collectionExplicitness : String?
	public var trackExplicitness : String?
	public var trackTimeMillis : Int?
	public var country : String?
	public var currency : String?
	public var primaryGenreName : String?
	public var contentAdvisoryRating : String?
	public var shortDescription : String?
	public var longDescription : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Json4Swift_Base Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [MJData]
    {
        var models:[MJData] = []
        for item in array
        {
            models.append(MJData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Json4Swift_Base Instance.
*/
	required public init?(dictionary: NSDictionary) {

		wrapperType = dictionary["wrapperType"] as? String
		kind = dictionary["kind"] as? String
		trackId = dictionary["trackId"] as? Int
		artistName = dictionary["artistName"] as? String
		trackName = dictionary["trackName"] as? String
		trackCensoredName = dictionary["trackCensoredName"] as? String
		trackViewUrl = dictionary["trackViewUrl"] as? String
		previewUrl = dictionary["previewUrl"] as? String
		artworkUrl30 = dictionary["artworkUrl30"] as? String
		artworkUrl60 = dictionary["artworkUrl60"] as? String
		artworkUrl100 = dictionary["artworkUrl100"] as? String
		trackRentalPrice = dictionary["trackRentalPrice"] as? Double
		trackHdRentalPrice = dictionary["trackHdRentalPrice"] as? Double
		releaseDate = dictionary["releaseDate"] as? String
		collectionExplicitness = dictionary["collectionExplicitness"] as? String
		trackExplicitness = dictionary["trackExplicitness"] as? String
		trackTimeMillis = dictionary["trackTimeMillis"] as? Int
		country = dictionary["country"] as? String
		currency = dictionary["currency"] as? String
		primaryGenreName = dictionary["primaryGenreName"] as? String
		contentAdvisoryRating = dictionary["contentAdvisoryRating"] as? String
		shortDescription = dictionary["shortDescription"] as? String
		longDescription = dictionary["longDescription"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.wrapperType, forKey: "wrapperType")
		dictionary.setValue(self.kind, forKey: "kind")
		dictionary.setValue(self.trackId, forKey: "trackId")
		dictionary.setValue(self.artistName, forKey: "artistName")
		dictionary.setValue(self.trackName, forKey: "trackName")
		dictionary.setValue(self.trackCensoredName, forKey: "trackCensoredName")
		dictionary.setValue(self.trackViewUrl, forKey: "trackViewUrl")
		dictionary.setValue(self.previewUrl, forKey: "previewUrl")
		dictionary.setValue(self.artworkUrl30, forKey: "artworkUrl30")
		dictionary.setValue(self.artworkUrl60, forKey: "artworkUrl60")
		dictionary.setValue(self.artworkUrl100, forKey: "artworkUrl100")
		dictionary.setValue(self.trackRentalPrice, forKey: "trackRentalPrice")
		dictionary.setValue(self.trackHdRentalPrice, forKey: "trackHdRentalPrice")
		dictionary.setValue(self.releaseDate, forKey: "releaseDate")
		dictionary.setValue(self.collectionExplicitness, forKey: "collectionExplicitness")
		dictionary.setValue(self.trackExplicitness, forKey: "trackExplicitness")
		dictionary.setValue(self.trackTimeMillis, forKey: "trackTimeMillis")
		dictionary.setValue(self.country, forKey: "country")
		dictionary.setValue(self.currency, forKey: "currency")
		dictionary.setValue(self.primaryGenreName, forKey: "primaryGenreName")
		dictionary.setValue(self.contentAdvisoryRating, forKey: "contentAdvisoryRating")
		dictionary.setValue(self.shortDescription, forKey: "shortDescription")
		dictionary.setValue(self.longDescription, forKey: "longDescription")

		return dictionary
	}

}
