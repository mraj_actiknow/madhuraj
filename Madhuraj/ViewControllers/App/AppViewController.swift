//
//  AppViewController.swift
//  Madhuraj
//
//  Created by Madhuraj on 26/07/21.
//

import UIKit

class AppViewController: UIViewController {

    @IBOutlet weak var navigationBar: NavBar!
    @IBOutlet weak var mjTableView: MJTableView!
    
    let appVm = AppViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.titleText = "SONGS"
        self.appVm.getList(target: self)
        // Do any additional setup after loading the view.
    }
    


}
