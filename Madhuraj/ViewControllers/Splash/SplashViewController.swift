//
//  SplashViewController.swift
//  Madhuraj
//
//  Created by Madhuraj on 26/07/21.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.delay(sec: 3) {
            print("Delayed For 3 Sec!")
            self.startApp()
        }
    }
    
    func startApp(){
        let vc = UIStoryboard.init(name: "App", bundle: Bundle.main).instantiateViewController(withIdentifier: "AppViewController") as? AppViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
