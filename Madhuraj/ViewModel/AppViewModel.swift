//
//  AppViewModel.swift
//  Madhuraj
//
//  Created by Madhuraj on 26/07/21.
//

import Foundation

class AppViewModel {
    
    func getList(target: AppViewController) {
        let parameter = [:] as [String : AnyObject]
        
        APIServices.sharedInstance.getList(params: parameter) { (response, status) in
            guard status else {
                Toast.show(message: Constants.ErrorMessage.unknown)
                return
            }
            target.mjTableView.mjlList = response
        
        }
    }
}
